console.log("Add (1 point)");
function addNumber(a, b) {
    return a + b;
}
console.log(addNumber(2, 3));

console.log("Multiply (2 points)");
function multiplyNumber(num1, num2) {
    let product= 0;
    for (let i = 1; i <= num2; i++) {
        product = addNumber(product, num1)
    }
    return product;
}
console.log(multiplyNumber(3, 3));

console.log("Power (2 points) ");
// function squareNumber(x, n) {
//     return (x ** n);
// }
function squareNumber(x, n){
    let totalPowerEqual = x;
    for (i =1; i < n; i++){
        totalPowerEqual = multiplyNumber(totalPowerEqual, x);
    }
    return totalPowerEqual;
}

console.log(squareNumber(2, 8));

console.log("Factorial");
function factorial(a) {
    let factorial = a;
    // for (let i = a; i >= 1; i--) {
    //     factorial = factorial * i;
    // }
    for (let i = a - 1; i >= 1 ; i--){
        factorial = multiplyNumber(factorial, i)
    }
    return factorial;
}

console.log(factorial(4));

console.log("Fibonacci");
function fibonacci(n) {
    const num = [0, 1];
    for (let i = 2; i <= n; i++) {
        let a = num[i - 1];
        let b = num[i - 2];
        num.push(a + b);
    }
    return num[n-1];
}
console.log(fibonacci(8));